<?php 
/**
 *
 * @author          Horacio Romero Mendez (angelos)
 * @License         Copyleft 2012
 * @since           19 Julio 2012 13:12:16
 * @Internal        GNU/Linux Mint 13 Maya Desktop
 *
 * 
 */
 
//include_once("nucleo.php");
//include_once("bitacora.php");

 
class conMy  {
    private static $host;
    private static $user;
    private static $pswd;
    private static $base;
       
    public static  $v = false;
    public static $link;
    private static $commit = 0; 
	
	
    
    private static function conectar($utf8){
        self::$host = "127.0.0.1";
        self::$user = "root";
        self::$pswd = "";
               
        self::$base = "agenda";
        
        self::$link=new mysqli(self::$host, self::$user, self::$pswd, self::$base);

        if (mysqli_connect_errno()) {
            //nucleo::addConsola("Base de datos", "<pre>Connect failed: %s", mysqli_connect_error()."</pre>");
            throw new Exception("Imposible conectar a la base de datos");
            exit();
        }           
       
       if ($utf8)
            self::$link->set_charset("utf8");
        //return $link;   
    }   

	
	public static function debug($v = false){
        self::$v = $v;
    }
    
    
    public static function begin(){
        try {
                
            self::$commit = 1;
            self::conectar(true);
            self::$link->autocommit(FALSE);
            
        }catch (Exception $e){
            throw new Exception ($e->getMessage());
        }
    }
    
    
    public static function commit(){
        try {
            self::$link->commit();
            self::$commit = 0;
            self::$link->autocommit(TRUE);
            self::cerrar();
        }catch (Exception $e){
            throw new Exception ($e->getMessage());
        }
    }
    
    
    public static function cerrar(){
        self::$link->close();
        self::$link = NULL;
        self::$commit = 0;
    }
	
    
    public static function sql($sql, $auth = "", $utf8 = true){
        $error      = 0;
        $accion     = explode(" ", str_replace("    ", " ", trim($sql)));

        switch ($accion[0]){
            case "INSERT":
                $accionNum  = 1; 
            break;
            case "UPDATE":
                $accionNum  = 2; 
            break;
            case "DELETE":
                $accionNum  = 3; 
            break;
            case "SELECT":
                $accionNum  = 4;
            break;
            case "BEGIN":
                $accionNum  = 5;
            break;
            case "COMMIT":
                $accionNum  = 6;
            break;
            case "ROLLBACK":
                $accionNum  = 7;
            break;
            default :
                $accionNum  = 0;
            break;
        }

        
        if (self::$v){
            $sql2 = str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $sql);
            //nucleo::addConsola("Base de datos", $sql2);    
        }
        
        try {
            if (is_null(self::$link))
                self::conectar($utf8);
            
        } catch (Exception $e) {
            throw new Exception("Error fatal: {$e->getMessage()}");
            exit();
        }
            
        if ($accionNum == 1) {
            $id = 0;    //SI NO SE ESPECIFICA TABLA Y CAMPO, SE REGRESA UN 0
            try {
                
                if (!$result   = self::$link->query($sql))  throw new Exception("Error al procesar peticion", 1);
                
                
                $id = self::$link->insert_id;
                
                if (!self::$commit)
                    self::cerrar();

                return $id;
                
            } catch(Exception $e) {

                //throw new Exception("Error ".bitacora::agregarError($sql, self::$link->errno." - ".self::$link->error, "MySQL", $auth)." en la consulta.");
                echo("Error ".self::$link->errno." - ".self::$link->error." en la consulta.");
                self::$link->rollback();
                //self::cerrar();
                
            }
        } else { 
            if ($result    = self::$link->query($sql)) {
                if (!self::$commit)
                    self::cerrar();
                return $result;
            } else {
                //throw new Exception("Error ".bitacora::agregarError($sql, self::$link->errno." - ".self::$link->error, "MySQL", $auth)." en la consulta.");
                echo("Error ".self::$link->errno." - ".self::$link->error." en la consulta.");
                //self::cerrar();
            }
        }
    }
}
 ?>
