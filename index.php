<?php
error_reporting(E_ALL);
        ini_set('display_errors', true);
        ini_set('html_errors', true);
 
include_once("conexion.php");
  
?>

<html>
<head>
<title>Agenda</title>
<script>
function target_popup(form) {
    window.open('', 'formpopup', 'width=1080,height=280,resizeable=0,scrollbars=0,top='+parseInt(((screen.height) / 2) - 140)+', left='+parseInt(((screen.width) / 2) - 540)+'');
    form.target = 'formpopup';
}
</script>
<link type="text/css" rel="stylesheet" href="css/estilo.css"/>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
</head>
<body>

<div class="divPrincipal">
<header class="bodyHeader"></header>
<content class="bodyContent">
    <div class="divCampos">       
        <form method="post" action="agregar.php" name="form">
            <label>* Nombre:</label><input type="text" size="18" name="contacto_nombre" required="required"/>
            <label>* Apellido Paterno:</label><input type="text" size="18" name="contacto_apaterno" required="required"/>
            <label>Apellido Materno:</label><input type="text" size="18" name="contacto_amaterno"/>
            <label>* Telefono:</label><input type="tel" size="16" maxlength="16" name="contacto_telefono" pattern="[0-9 ]+" title="Ingresa solo n&uacute;meros" placeholder="ej: 044 555 555 5555" required="required"/>
            <label>* Direcci&oacute;n:</label><input type="text" size="75" name="contacto_direccion" required="required"/>
            <label>* Num. Ext.:</label><input type="text" size="5" name="contacto_num_ext" required="required"/>
            <label>Num. Int.:</label><input type="text" size="5" name="contacto_num_int"/>
            <label>* C.P.</label><input type="text" size="5"  maxlength="5" name="contacto_cp" pattern="[0-9]{5}" title="Cinco digitos del c&oacute;digo postal." placeholder="ej: 93400" required="required"/>
            <label>* Foto</label><input type="file" name="contacto_foto" required="required"/>
            <input type="submit" name="submit" value="Agregar"/> (*) Campos requeridos
        </form>    
    </div>
    <hr/>
<div class="divCampos">     
        <form method="get" action="busqueda.php" name="form" onsubmit="target_popup(this)">
            <label>* Introducir nombre a buscar:</label><input type="text" size="18" name="contacto_nombre" required="required"/>
            <input type="submit" name="submit" value="Buscar"/> (*) Campo requeridos
        </form>
 </div>
 <br />
<?php include_once("registrados.php");?>

    </div>
    
</content>


</body>
</html>
