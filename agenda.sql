-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-03-2015 a las 16:48:25
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `agenda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
`cvecontacto` int(11) NOT NULL,
  `contacto_nombre` varchar(50) NOT NULL,
  `contacto_apaterno` varchar(50) NOT NULL,
  `contacto_amaterno` varchar(50) DEFAULT NULL,
  `contacto_telefono` varchar(10) NOT NULL,
  `contacto_direccion` varchar(250) NOT NULL,
  `contacto_num_ext` varchar(10) NOT NULL,
  `contacto_num_int` varchar(10) DEFAULT NULL,
  `contacto_cp` varchar(5) NOT NULL,
  `contacto_foto` blob NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`cvecontacto`, `contacto_nombre`, `contacto_apaterno`, `contacto_amaterno`, `contacto_telefono`, `contacto_direccion`, `contacto_num_ext`, `contacto_num_int`, `contacto_cp`, `contacto_foto`) VALUES
(1, 'luis', 'cortÃ©s', 'Davila', '0442721392', 'mi calle', '22', '', '12345', 0x4b6f616c612e6a7067),
(2, 'l', '1', '1', '1', '1', '1', '1', '11111', 0x4b6f616c612e6a7067),
(3, 'beto', '2', '2', '2', '2', '2', '2', '22222', 0x4368727973616e7468656d756d2e6a7067),
(4, 'Luis', 'CortÃ©s', '', '2721392381', 'mi calle', '245 a', '1-B', '12345', 0x4c69676874686f7573652e6a7067);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
 ADD PRIMARY KEY (`cvecontacto`), ADD UNIQUE KEY `cvecontacto` (`cvecontacto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
MODIFY `cvecontacto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
