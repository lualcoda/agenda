<?php
//LLAMAR A LA BIBLIOTECA
include_once("libs/conMy.php");
    
?>

<html>
<head>
<title>Agenda</title>
<script>
  function CenterWindow(windowWidth, windowHeight, windowOuterHeight, url, wname, features) {
    var centerLeft = parseInt((window.screen.availWidth - windowWidth) / 2);
    var centerTop = parseInt(((window.screen.availHeight - windowHeight) / 2) - windowOuterHeight);
 
    var misc_features;
    if (features) {
      misc_features = ', ' + features;
    }
    else {
      misc_features = ', status=no, location=no, scrollbars=yes, resizable=yes';
    }
    var windowFeatures = 'width=' + windowWidth + ',height=' + windowHeight + ',left=' + centerLeft + ',top=' + centerTop + misc_features;
    var win = window.open(url, wname, windowFeatures);
    win.focus();
    return win;
  }
</script>
<link type="text/css" rel="stylesheet" href="css/estilo.css"/>
</head>
<body>

    <div class="contentTable">
    <content>
    <table>

<thead>
    <tr>
    <th>ID</th>
    <th>Nombre</th>
    <th>Apellido Paterno</th>
    <th>Apellido Materno</th>
    <th>Telefono</th>
    <th>Direccion</th>
    <th>Num. Ext.</th>
    <th>Num. Int.</th>
    <th>C.P.</th>
    <th>Foto</th>
    <th>Editar</th>
    <th>Eliminar</th>
    </tr>
</thead>

<tbody>

<?php
/*
// Create connection
$conn = new mysqli($dbhost, $dbuser, $dbpassword, $database);
// Check connection
if ($conn->connect_error) {
     die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT * FROM contacto";
        $result = $conn->query($sql);

if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
*/

//POR EL MOMENTO PONGO LA CONSULTA AQUI, PERO DEBE IR EN LA CAPA DE MODELO, LLAMADO A SU VEZ POR EL CONTROLADOR
$query = "	SELECT	*
			FROM	contacto";
$result = conMy::sql($query);

if ($result->num_rows > 0) {

// DE PREFERENCIA QUE SEA UN OBJETO Y NO UN ARREGLO ASOCIATIVO
// ANTES <?php echo "<td>" . $row["cvecontacto"]. "</td>";
// AHORA <?php echo "<td>" . $row->cvecontacto. "</td>";
while ($row = $result->fetch_object()){
?>
   <tr>
    <td>
		<?php //NUNCA HAGAS QUE PHP ESCRIBA HTML, ES GASTO INNECESARIO DE RECURSO, SOLO CASOS ESPECIALES ?>
		<?php //QUE PHP SOLO ESCRIBA LAS VARIABLES ?>
		<?php echo $row->cvecontacto;?>
	</td>
	<?php //EN LUGAR DE QUE ESCRIBA LOS TAGS CON EL ECHO ?>
    <?php echo "<td>" . $row->contacto_nombre. "</td>";?>
    <?php echo "<td>" . $row->contacto_apaterno. "</td>";?>
    <?php echo "<td>" . $row->contacto_amaterno. "</td>";?>
    <?php echo "<td>" . $row->contacto_telefono. "</td>";?>
    <?php echo "<td>" . $row->contacto_direccion. "</td>";?>
    <?php echo "<td>" . $row->contacto_num_ext. "</td>";?>
    <?php echo "<td>" . $row->contacto_num_int. "</td>";?>
    <?php echo "<td>" . $row->contacto_cp. "</td>";?>
    <?php echo "<td>" . $row->contacto_foto. "</td>";?>
    <td><a href="javascript:void(0)" onclick="CenterWindow(1080,280,50,'modificar.php?cvecontacto=<?php echo $row->cvecontacto;?>','Actualizar');">Editar</a></td>
    <td><a href="borrar.php?cvecontacto=<?php echo $row->cvecontacto;?>" onclick="return confirm('Al borrar el registro no se podra recuperar la informaci&oacute;n, &iquest;Seguro que desea continuar?')">Borrar</a></td>
    </tr>
       
<?php }
} else {
     echo "No hay registros";
}

?>

    </tbody>
    </table>
    </div>
</content>
<footer class="bodyFooter">
<p class="pFooter">Pr&aacute;ctica Luis Cort&eacute;s</p>
</footer>


</body>
</html>
